﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusService.Models;

namespace BusService
{
    public interface IArrivalsService {

        List<RouteArrivals> GetUpcomingArrivals(DateTimeOffset timeOffset, int stopNumber, int numArrivalTimes);

        Task<List<RouteArrivals>> GetUpcomingArrivalsAsync(DateTimeOffset time, int stopNumber, int numArrivalTimes);
    }
}

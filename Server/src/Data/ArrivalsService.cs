﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusService.Models;

namespace BusService
{
    public class ArrivalsService : IArrivalsService {

        readonly int[] validStops = Enumerable.Range(1, 10).ToArray();
        const int circuitInterval = 15;
        const int stopInterval = 2;
        const int routeInterval = 2;
        const int numRoutes = 3;

        public List<RouteArrivals> GetUpcomingArrivals(DateTimeOffset time, int stopNumber, int numArrivalTimes) {
            // check for valid stop
            if ( !validStops.Any(s => s == stopNumber) ) {
                return null;
            }

            // zero out everything smaller than minutes
            time = time.AddTicks(-1 * time.Ticks % TimeSpan.TicksPerMinute);

            int mins = time.Minute;
            List<RouteArrivals> routeArrivals = new List<RouteArrivals>();

            for (int route=1; route<=numRoutes; ++route) {
                int routeAdjustedTime = (60 + mins - routeInterval*(route-1)) % 60;
                int stopAdjustedTime = (60 + routeAdjustedTime - stopInterval*(stopNumber-1)) % 60;

                int minsUntil = circuitInterval - (stopAdjustedTime % circuitInterval);
                if (minsUntil == circuitInterval) minsUntil = 0;

                var arrivals = new Arrival[numArrivalTimes];
                for (int i=0; i<numArrivalTimes; ++i) {
                    arrivals[i] = new Arrival {
                        Time = time.AddMinutes(minsUntil + i*circuitInterval),
                        MinutesUntil = minsUntil + i*circuitInterval };
                }

                routeArrivals.Add(
                    new RouteArrivals { Route = route.ToString(), Arrivals = arrivals }
                );
            }

            return routeArrivals;
        }

        public Task<List<RouteArrivals>> GetUpcomingArrivalsAsync(DateTimeOffset time, int stopNumber, int numArrivalTimes) {
            // just call the synchronous method asynchronously
            return Task.FromResult(GetUpcomingArrivals(time, stopNumber, numArrivalTimes));
        }
    }
}

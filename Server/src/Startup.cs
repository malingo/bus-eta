﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace BusService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore().AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");
            services.AddMvc();
            services.AddApiVersioning();

            services.AddSwaggerGen(opts => {
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                foreach (var desc in provider.ApiVersionDescriptions) {
                    opts.SwaggerDoc(desc.GroupName, CreateInfoForApiVersion(desc));
                }
                opts.OperationFilter<SwaggerDefaultValues>();
            });

            services.AddScoped<IArrivalsService,ArrivalsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(opts => {
                foreach (var desc in provider.ApiVersionDescriptions) {
                    opts.SwaggerEndpoint($"/swagger/{desc.GroupName}/swagger.json", desc.GroupName.ToUpperInvariant());
                }
            });

            app.UseMvc();
        }

        Info CreateInfoForApiVersion(ApiVersionDescription desc)
        {
            var info = new Info() {
                Title = $"Bus Arrival Time API {desc.ApiVersion}",
                Version = desc.ApiVersion.ToString(),
                Description = "See upcoming bus arrival times.",
                Contact = new Contact() { Name = "Dave Kaminski", Email = "dave@malingo.net" },
                License = new License() { Name = "MIT", Url = "https://opensource.org/licenses/MIT" }
            };

            if (desc.IsDeprecated) {
                info.Description += "This API version has been deprecated.";
            }

            return info;
        }
    }
}

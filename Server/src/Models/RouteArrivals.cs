﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusService.Models
{
    public class RouteArrivals
    {
        public string Route { get; set; }

        public Arrival[] Arrivals { get; set; }
    }
}

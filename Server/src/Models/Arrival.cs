using System;
using System.Collections.Generic;
using System.Linq;

namespace BusService.Models
{
    public class Arrival
    {
        public DateTimeOffset Time { get; set; }

        public int MinutesUntil { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace BusService.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Produces("application/json")]
    public class StopsController : Controller
    {
        readonly IArrivalsService _arrivalsSvc;

        const int numArrivalTimes = 2;

        public StopsController(IArrivalsService arrivalsService) {
            _arrivalsSvc = arrivalsService;
        }

        [HttpGet("{stopNum:int}/arrivals")]
        public async Task<IActionResult> Get(int stopNum)
        {
            var now = DateTimeOffset.Now;
            try {
                // log it
                var arrivals = await _arrivalsSvc.GetUpcomingArrivalsAsync(now, stopNum, numArrivalTimes);
                if (arrivals == null) {
                    return NotFound();
                }
                else {
                    return Ok(arrivals);
                }
            }
            catch (Exception ex) {
                // log it
                return StatusCode(500);
            }
        }
    }
}

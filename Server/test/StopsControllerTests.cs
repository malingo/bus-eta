using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using Xunit;
using BusService;
using BusService.Controllers;
using BusService.Models;

namespace BusService.Tests {

    public class StopsControllerTests {

        [Fact]
        // Test the logic of the controller receiving a null result from the service
        public async Task UndefinedStopReturnNotFound() {
            //Given
            var _svc = Substitute.For<IArrivalsService>();
            // Force the arrivals service to return null, representing a bogus stop number
            _svc.GetUpcomingArrivalsAsync(Arg.Any<DateTimeOffset>(), Arg.Any<int>(), Arg.Any<int>())
                .Returns((List<RouteArrivals>)null);

            var controller = new StopsController(_svc);

            //When
            var result = await controller.Get(Arg.Any<int>());

            //Then
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        // Check error handling
        public async Task ErrorFromServiceReturnsErrorResponse() {
            //Given
            var _svc = Substitute.For<IArrivalsService>();
            // Force the service to throw an exception
            _svc.GetUpcomingArrivalsAsync(Arg.Any<DateTimeOffset>(), Arg.Any<int>(), Arg.Any<int>())
                .Returns<List<RouteArrivals>>(_ => { throw new Exception(); });

            var controller = new StopsController(_svc);

            //When
            var result = await controller.Get(Arg.Any<int>());

            //Then
            Assert.IsType<StatusCodeResult>(result);
            int statusCode = ((StatusCodeResult)result).StatusCode;
            Assert.Equal(500, statusCode);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        // Ensure that the service gets called with the stop number from the request
        // and also specifying the correct number of upcoming arrival times
        public async Task CallServiceWithProperArgs(int stopNum) {
            //Given
            var _svc = Substitute.For<IArrivalsService>();
            var controller = new StopsController(_svc);

            //When
            var result = await controller.Get(stopNum);

            //Then
            await _svc.Received().GetUpcomingArrivalsAsync(
                Arg.Any<DateTimeOffset>(),
                stopNum,
                2);
        }

        [Fact]
        public async Task EnsureProperResponseOnSuccess() {
            //Given
            var _svc = Substitute.For<IArrivalsService>();
            _svc.GetUpcomingArrivalsAsync(Arg.Any<DateTimeOffset>(), Arg.Any<int>(), Arg.Any<int>())
                .Returns(GetArrivals());
            var controller = new StopsController(_svc);

            //When
            var result = await controller.Get(1);

            //Then
            Assert.IsType<OkObjectResult>(result);
            var responseContent = ((OkObjectResult)result).Value;
            Assert.IsType<List<RouteArrivals>>(responseContent);
        }

        List<RouteArrivals> GetArrivals() {
            return
                new List<RouteArrivals> {
                    new RouteArrivals { Route = "1", Arrivals = new [] {
                            new Arrival { Time = DateTimeOffset.Now, MinutesUntil = 0 },
                            new Arrival { Time = DateTimeOffset.Now.AddMinutes(15), MinutesUntil = 15 }
                        }
                    }
                };
        }
    }
}

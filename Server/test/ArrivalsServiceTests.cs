using System;
using System.Collections.Generic;
using Xunit;
using BusService;
using BusService.Models;
using System.Globalization;

namespace BusService.Tests
{
    public class ArrivalsServiceTests
    {
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(11)]
        public void CheckInvalidStop(int stopNum) {
            var svc = new ArrivalsService();
            Assert.Null(svc.GetUpcomingArrivals(DateTimeOffset.Now, stopNum, 1));
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void CheckTimes(DateTimeOffset time, int stopNum, int numArrivals, List<RouteArrivals> expected) {
            var svc = new ArrivalsService();
            var actual = svc.GetUpcomingArrivals(time, stopNum, numArrivals);

            Assert.Equal(expected.Count, actual.Count);
            for (int i=0; i<expected.Count; ++i) {
                Assert.Equal(expected[i].Route, actual[i].Route);
                Assert.Equal(expected[i].Arrivals.Length, actual[i].Arrivals.Length);

                for (int k=0; k<expected[i].Arrivals.Length; ++k) {
                    Assert.Equal(expected[i].Arrivals[k].Time, actual[i].Arrivals[k].Time);
                    Assert.Equal(expected[i].Arrivals[k].MinutesUntil, actual[i].Arrivals[k].MinutesUntil);
                }
            }
        }

        static DateTimeOffset Date(string dateString) {
            string timePattern = "yyyy-MM-dd'T'HH:mm:ssK";
            return DateTimeOffset.ParseExact(dateString, timePattern, CultureInfo.InvariantCulture);
        }

        public static IEnumerable<object[]> TestData() {
            /****************************
             ** Starting at midnight   **
             ****************************/
            var requestTime = Date("2018-02-01T00:00:00-08:00");

            var route1next1 = Date("2018-02-01T00:00:00-08:00");
            var route1next2 = Date("2018-02-01T00:15:00-08:00");

            var route2next1 = Date("2018-02-01T00:02:00-08:00");
            var route2next2 = Date("2018-02-01T00:17:00-08:00");

            var route3next1 = Date("2018-02-01T00:04:00-08:00");
            var route3next2 = Date("2018-02-01T00:19:00-08:00");

            yield return new object[] {
                requestTime,
                1, /* stop number */
                2, /* num of arrivals */
                new List<RouteArrivals> {
                    new RouteArrivals { Route = "1", Arrivals = new [] {
                            new Arrival { Time = route1next1, MinutesUntil = route1next1.Subtract(requestTime).Minutes },
                            new Arrival { Time = route1next2, MinutesUntil = route1next2.Subtract(requestTime).Minutes }
                        }
                    },
                    new RouteArrivals { Route = "2", Arrivals = new [] {
                            new Arrival { Time = route2next1, MinutesUntil = route2next1.Subtract(requestTime).Minutes },
                            new Arrival { Time = route2next2, MinutesUntil = route2next2.Subtract(requestTime).Minutes }
                        }
                    },
                    new RouteArrivals { Route = "3", Arrivals = new [] {
                            new Arrival { Time = route3next1, MinutesUntil = route3next1.Subtract(requestTime).Minutes },
                            new Arrival { Time = route3next2, MinutesUntil = route3next2.Subtract(requestTime).Minutes }
                        }
                    }
                }
            };

            /**********************************
             ** Wrapping around end of month **
             **********************************/
            requestTime = Date("2018-01-31T23:59:00-04:00");
            route1next1 = Date("2018-02-01T00:00:00-04:00");
            route2next1 = Date("2018-02-01T00:02:00-04:00");
            route3next1 = Date("2018-02-01T00:04:00-04:00");

            yield return new object[] {
                requestTime,
                1, /* stop number */
                1, /* num of arrivals */
                new List<RouteArrivals> {
                    new RouteArrivals { Route = "1", Arrivals = new [] {
                            new Arrival { Time = route1next1, MinutesUntil = route1next1.Subtract(requestTime).Minutes }
                        }
                    },
                    new RouteArrivals { Route = "2", Arrivals = new [] {
                            new Arrival { Time = route2next1, MinutesUntil = route2next1.Subtract(requestTime).Minutes }
                        }
                    },
                    new RouteArrivals { Route = "3", Arrivals = new [] {
                            new Arrival { Time = route3next1, MinutesUntil = route3next1.Subtract(requestTime).Minutes }
                        }
                    }
                }
            };

            /***************************************************
             ** Some other time of day, different stop number **
             ***************************************************/
            requestTime = Date("2018-01-31T03:29:00-04:00");

            route1next1 = Date("2018-01-31T03:32:00-04:00");
            route1next2 = Date("2018-01-31T03:47:00-04:00");
            var route1next3 = Date("2018-01-31T04:02:00-04:00");

            route2next1 = Date("2018-01-31T03:34:00-04:00");
            route2next2 = Date("2018-01-31T03:49:00-04:00");
            var route2next3 = Date("2018-01-31T04:04:00-04:00");

            route3next1 = Date("2018-01-31T03:36:00-04:00");
            route3next2 = Date("2018-01-31T03:51:00-04:00");
            var route3next3 = Date("2018-01-31T04:06:00-04:00");

            yield return new object[] {
                requestTime,
                2, /* stop number */
                3, /* num of arrivals */
                new List<RouteArrivals> {
                    new RouteArrivals { Route = "1", Arrivals = new [] {
                            new Arrival { Time = route1next1, MinutesUntil = route1next1.Subtract(requestTime).Minutes },
                            new Arrival { Time = route1next2, MinutesUntil = route1next2.Subtract(requestTime).Minutes },
                            new Arrival { Time = route1next3, MinutesUntil = route1next3.Subtract(requestTime).Minutes }
                        }
                    },
                    new RouteArrivals { Route = "2", Arrivals = new [] {
                            new Arrival { Time = route2next1, MinutesUntil = route2next1.Subtract(requestTime).Minutes },
                            new Arrival { Time = route2next2, MinutesUntil = route2next2.Subtract(requestTime).Minutes },
                            new Arrival { Time = route2next3, MinutesUntil = route2next3.Subtract(requestTime).Minutes }
                        }
                    },
                    new RouteArrivals { Route = "3", Arrivals = new [] {
                            new Arrival { Time = route3next1, MinutesUntil = route3next1.Subtract(requestTime).Minutes },
                            new Arrival { Time = route3next2, MinutesUntil = route3next2.Subtract(requestTime).Minutes },
                            new Arrival { Time = route3next3, MinutesUntil = route3next3.Subtract(requestTime).Minutes }
                        }
                    }
                }
            };
        }

    }
}

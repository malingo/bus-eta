﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace BusClient
{
    class Program
    {
        const string apiUrlBase = "http://localhost:5000";
        const int intervalMills = 60 * 1000;
        static HttpClient httpClient;
        static Timer timer;

        static void Main(string[] args) {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(apiUrlBase);

            // get the arrival times immediately
            Task.Run(() => DisplayBusTimes());

            // then set up a timer to get the arrival times every minute
            timer = new Timer(intervalMills);
            timer.Elapsed += async (o,e) => await DisplayBusTimes();
            timer.Start();

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }

        static async Task DisplayBusTimes() {
            await GetForStop(1);
            await GetForStop(2);
        }

        static async Task GetForStop(int stopNum) {
            Console.WriteLine();
            Console.WriteLine($"Stop {stopNum}:");
            try {
                var resp = await httpClient.GetAsync($"/api/v1/stops/{stopNum}/arrivals");
                var content = await resp.Content.ReadAsStringAsync();
                var stops = JArray.Parse(content);
                foreach (dynamic stop in stops) {
                    Console.WriteLine($"Route {stop.route} in {stop.arrivals[0].minutesUntil} mins and {stop.arrivals[1].minutesUntil} mins");
                }
            }
            catch {
                Console.WriteLine("Error while requesting arrival times.");
            }
        }
    }
}

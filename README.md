# Bus ETA Project

To run the application, first start the server then start the client that calls the server. The server and client both target the dotnet core framework and can be run with the `dotnet` command-line utility.

### Server

In one console window, enter this command:
```
$ dotnet run --project Server/src/BusService.csproj
```
That will start the server and will log requtests and errors to stdout. View Swagger documentation for the API in a browser at http://localhost:5000/swagger

Press `Ctrl+C` to shut down the server.

### Client

In a separate console window, enter this command:
```
$ dotnet run --project Client/BusClient.csproj
```
That will start the client and will request the upcoming arrival times for stops 1 and 2 every 60 seconds.

Press `Enter` to shut down the client.

### Tests

To run the tests, enter this command:
```
$ dotnet test Server/test/BusService.Tests.csproj
```
